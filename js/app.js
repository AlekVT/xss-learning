window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB
/*
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"};
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
*/

if (!window.indexedDB)
{
    window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
}

const login = [
    { username: 'jpodesta', password: 'p@ssw0rd', admin: true }
]

const candidates = [
    { name: 'Donald Trump', id: 0},
    { name: 'Hillary Clinton', id: 1},
    { name: 'Jill Stein', id: 2},
    { name: 'John McAfee', id: 3},
    { name: 'Bernie Sanders', id: 4},
    { name: 'Jeb Bush', id: 5 },
    { name: 'Ted Cruz', id: 6 }
];

const users = [
    { username: 'Mike P.', vote: 0, ip: '21a4:4345:432:b34:c432b:4321:b321:3453', banned: false},
    { username: 'Milo Y.', vote: 0, ip: '4321:4322:432:454:1234:1234:12354:3453', banned: true},
    { username: 'CTR', vote: 1, ip: '0000:4321:432:543:0000:5433:4323:3456', banned: false},
    { username: 'CNN', vote: 1, ip: '0000:4321:432:543:0000:5433:4323:3453', banned: false},
    { username: 'Jeb', vote: 3, ip: '0000:4321:432:543:0000:5433:4321:5436', banned: false}
];

const comments = [
    { content: "Vote Hillary you guys, it's Her turn!", userid: '0000:4321:432:543:0000:5433:4323:3453'},
    { content: "Build that wall!", userid: '4321:4322:432:454:1234:1234:12354:3453'},
    { content: "Trump eats babies.", userid: '0000:4321:432:543:0000:5433:4323:3456', responding_to: 1},
    { content: "I can confirm this.", userid: '0000:4321:432:543:0000:5433:4323:3453', responding_to: 2},
    { content: "How do I change my vote? I accidentally clicked McAfee...", userid: '0000:4321:432:543:0000:5433:4321:5436'},
    { content: "MAGA", userid: '21a4:4345:432:b34:c432b:4321:b321:3453'}
];

const dbName = 'Election2016';

var db;
var candidatesList;
var userip = '127.0.0.1';

// emulate the php get so we can abuse it later

var $_GET = {};
args = location.search.substr(1).split(/&/);
for (var i in args)
{
    var tmp = args[i].split(/=(.+)/);
    if (tmp[0] != '' && tmp.length != 2)
    {
        //$_GET[decodeURIComponent(tmp[0])] = decodeURIComponent(tmp.slice(1).join('').replace('+', ' '));
        $_GET[decodeURIComponent(tmp[0])] = decodeURIComponent(tmp[1]); // fixed sql injection, will probably break other stuff
    }
}

if (typeof($_GET.redirect) != 'undefined')
{
    /*let req = new XMLHttpRequest();
    //req.responseType = "document";
    req.open("GET", 'evil.js');
    req.onreadystatechange = function()
    {
        eval(req.responseText);
    }
    req.send();*/
    
    setTimeout(function()
    {
        location.replace('elect.html?something=lol%0d%0a%0d%0a<script>alert("lol")</script>');
    }, 1000);
    
    //window.location.replace($_GET.redirect);
}

// open/create the database
function openDb(dbversion = null, action = null)
{
    var request;
    
    if (dbversion != null)
    {
        request = window.indexedDB.open(dbName, dbversion);
    }
    else
    {
        request = window.indexedDB.open(dbName);
    }
    
    request.onerror = function(event)
    {
        alert('whoops, can\'t create a database, are you in private mode per chance? Error: ' + event.target.errorCode);
    };
    
    request.onsuccess = function(event)
    {
        db = event.target.result;
        showCandidates();
        showComments();
        checkGetAfterLoad();
        createHash();
    };
    
    request.onupgradeneeded = function(event)
    {
        db = event.target.result;
        
        switch (action)
        {
            case 'users':
            {
                createUsers();
                break;
            }
            case 'comments':
            {
                createComments();
                break;
            }
            case 'logins':
            {
                createLogins();
                break;
            }
            case 'candidates':
            default:
            {
                createCandidates();
                break;
            }
        }
    }
}

 // logins
function createLogins()
{
    let loginObjectStore = db.createObjectStore('logins', { autoIncrement : true });
    
    loginObjectStore.createIndex('username', 'username', { unique : true });
    
    loginObjectStore.transaction.oncomplete = function(event)
    {
        create('logins', login)
        .then(function()
        {
            db.close();
            window.location.reload();
        });
    };
};
    
// Comments
function showComments()
{
    let commentdiv = document.getElementById('comments');
    
    if (commentdiv.childElementCount === 0)
    {
        readAll('comments')
        .then(function(data)
        {
            let commentsArray = data;
            readAll('users')
            .then(function(data)
            {
                for (let i in commentsArray)
                {
                    let user = null;
                    for (let y in data)
                    {
                        if (data[y].ip === commentsArray[i].userid)
                        {
                            user = data[y];
                        }
                    }

                    let div = document.createElement('div');
                    div.className = 'comment';
                    let userName = document.createElement('h3');

                    let usernametext = '';
                    if (user.banned)
                    {
                        usernametext = 'banned - ';
                    }
                    userName.innerText = usernametext + user.username;
                    div.appendChild(userName);
                    let paragraph = document.createElement('p');
                    //innerHTML instead of innerText to make xss even easier
                    paragraph.innerHTML = commentsArray[i].content;
                    div.appendChild(paragraph);
                    
                    if (userip == user.ip)
                    {
                        let deletebox = document.createElement('input');
                        deletebox.type = 'button';
                        deletebox.onclick = function() { window.location.replace('elect.html?delete=' + commentsArray[i].id); };
                        deletebox.value = 'delete comment';
                        div.appendChild(deletebox);
                    }
                    
                    commentdiv.appendChild(div);
                }
                
                //here we use the evil function to pretend we actually did all this on the server and thus execute any inserted js
                let scripts = document.getElementsByTagName('script');
                for(x = 3;x < scripts.length;x++)
                {
                    eval(scripts[x].innerHTML);
                }
            });
        });
    }
};

function createComments()
{
    let commentObjectStore = db.createObjectStore('comments', { keyPath: 'id', autoIncrement : true });
    
    commentObjectStore.createIndex('content', 'content', { unique : false });
    commentObjectStore.createIndex('userid', 'userid', { unique : false });
    
    commentObjectStore.transaction.oncomplete = function(event)
    {
        create('comments', comments)
            .then(function()
            {
            db.close();
            openDb(++db.version, 'logins');
        });
    };
};

// Users
function createUsers()
{
    let userObjectStore = db.createObjectStore('users', { keyPath: 'ip', autoIncrement : true });

    userObjectStore.createIndex('username', 'username', { unique: true });
    userObjectStore.createIndex('ip', 'ip', { unique: true });

    userObjectStore.transaction.oncomplete = function(event)
    {
        create('users', users)
        .then(function()
        {
            db.close();
            openDb(++db.version, 'comments');
            //createComments();
        });
    }
};

// Candidates
function showCandidates()
{
    let options = document.getElementById('options');
    
    if (options.childElementCount === 0)
    {
        readAll('users')
        .then(function(users)
        {
            let voteTotal = 0;
            for (let y in users)
            {
                if (typeof(users[y].vote) != 'undefined')
                {
                    voteTotal++;
                }
            }
            
            readAll('candidates')
            .then(function(data) 
            {
                for (let i in data)
                {
                    let div = document.createElement('div');
                    let option = document.createElement('input');
                    let label = document.createElement('label');
                    
                    div.className = option.name = 'candidate';
                    option.type = 'radio';
                    option.required = true;
                    let votes = 0;
                    
                    for (let y in users)
                    {
                        if (users[y].vote == data[i].id)
                        {
                            votes++;
                        }
                    }
                    
                    label.innerText = data[i].name + ' votes: ' + votes;
                    label.htmlFor = option.id = option.value = data[i].id;
                    
                    div.appendChild(option);
                    div.appendChild(label);
                    options.appendChild(div);
                }
            });
        });
    }
};

function createCandidates()
{
    let canditObjectStore = db.createObjectStore('candidates', { keyPath: 'id', autoIncrement : true });
    canditObjectStore.createIndex('name', 'name', { unique: true });

    canditObjectStore.transaction.oncomplete = function(event)
    {
        create('candidates', candidates)
        .then(function()
        {
            db.close();
            openDb(++db.version, 'users');
        });
    };
};

window.onload = function()
{
    //document.getElementsByTagName('h1')[0].innerHTML = $_GET.testvar;
    
    // start
    openDb();
};

// select * from table
function readAll(table)
{
    return new Promise(function(resolve)
    {
        let transaction = db.transaction([table]);
        let objectStore = transaction.objectStore(table);
    
        let request = objectStore.getAll();
        
        request.onerror = function(event)
        {
            console.log("Looks like the get couldn't be gotten: " + event.target.errorCode);
        }
        
        request.onsuccess = function()
        {
            resolve(request.result);
        };
    })
};

// select by id
function readById(table, id)
{
    return new Promise(function (resolve)
    {
        let transaction = db.transaction([table]);
        let objectStore = transaction.objectStore(table);
        let request = objectStore.get(id);

        request.onerror = function(event)
        {
            console.log("Looks like the get couldn't be gotten: " + event.target.errorCode);
        };

        request.onsuccess = function()
        {
            console.log(request.result);
            resolve(request.result);
        };
    });
};

// select by value name
function readByValue(table, valuename, value)
{
    return new Promise(function (resolve)
    {
        let transaction = db.transaction([table]);
        let objectStore = transaction.objectStore(table);
        let index = objectStore.index(valuename);
        let request = index.get(value);
        
        request.onerror = function(event)
        {
            console.log("Looks like the get couldn't be gotten: " + event.target.errorCode);
        };

        request.onsuccess = function()
        {
            resolve(request.result);
        };
    });
}

// insert into table
function create(table, values)
{
    return new Promise(function(resolve, error)
    {
        let transaction = db.transaction(table, 'readwrite');
        
        transaction.oncomplete = function(event)
        {
            resolve(event.result);
        };

        transaction.onerror = function(event)
        {
            console.log('something went wrong with the transaction: ' + event.target.errorCode);
            error(event);
        };

        let objectStore = transaction.objectStore(table);

        for (let i in values)
        {
            var request = objectStore.add(values[i]);
        }
        
        request.onsuccess = function(event)
        {
            console.log("inserted " + event.target.result + " items.");
        }
    });
};

// delete from table
function deleteOne(table, id)
{
    return new Promise(function(resolve, error)
    {
        let request = db.transaction([table], 'readwrite')
        .objectStore(table)
        .delete(id);
        
        request.onsuccess = function(event)
        {
            resolve();
        }
    });
};

// update row
function updateOne(table, id)
{
    return new Promise(function()
    {
        let transaction = db.transaction([table], 'readwrite');
        let objectStore = transaction.objectStore(table);
        let request = objectStore.get(id);

        request.onerror = function(event)
        {
            console.log("Looks like the get couldn't be gotten: " + event.target.errorCode);
        };

        request.onsuccess = function(event)
        {
            let data = event.target.result;
            data.username = username;
                            
            let requestUpdate = objectStore.put(data);
            requestUpdate.onerror = function(event)
            {
                console.log("it didn't want to update: " + event.target.errorCode);
            };
            requestUpdate.onsuccess = function(event)
            {
                deleteOne(table, id);
            };
        };
    });
};

function vote(vote)
{
    alert('thanks for voting');
    readByValue('users', 'ip', userip)
    .then(function(data)
    {
        if (typeof(data) != 'undefined')
        {
            let transaction = db.transaction('users', 'readwrite');
            let objectStore = transaction.objectStore('users');
            let request = objectStore.get(userip);

            request.onerror = function(event)
            {
                console.log("Looks like the get couldn't be gotten: " + event.target.errorCode);
            };

            request.onsuccess = function(event)
            {
                console.log(event.target.result);
                let data = event.target.result;
                data.vote = vote;

                let requestUpdate = objectStore.put(data);

                requestUpdate.onerror = function(event)
                {
                    console.log("it didn't want to update: " + event.target.errorCode);
                };
                requestUpdate.onsuccess = function(event)
                {
                    window.location.replace('elect.html');
                };
            };
        }
        else
        {
            create('users', [{ vote: vote, ip: userip, banned: false }]);
            window.location.replace('elect.html');
        }
    });
}
                                               
function postComment()
{
    let username = document.getElementById('username').value;
    let comment = document.getElementById('comment').value;
    
    if ((username == '' || typeof(username) == 'undefined') || (comment == '' || typeof(comment) == 'undefined' || comment == false))
    {
        alert('you must fill in your username and a comment to be able to post');
        return;
    }
    
    readByValue('users', 'ip', userip)
    .then(function(data)
    {
        if (typeof(data) != 'undefined')
        {
            if (typeof(data.username) == 'undefined' || data.username == '' || data.username == null)
            {
                postCommentToDb(username, comment, false, true);
            }
            else
            {
                postCommentToDb(username, comment);
            }
        }
        else
        {
            create('users', [{ username: username, vote: null, ip: userip, banned: false }])
            .then(function(data)
            {
                postCommentToDb(username, comment, true);
            })
            .catch(function(data)
            {
                console.log("A user with this name already exists.");
            });
        }
    });
};

function postCommentToDb(username, comment, newUser = false, newname = false)
{
    readByValue('users', 'ip', userip)
    .then(function(data)
    {
        let hash = document.getElementById('hash');
        
        if ((hash != null && (username + userip).hashCode() == hash.value) || (newname == true && (data.username + userip).hashCode() == hash.value))
        {
            create('comments', [{ content: comment, userid: userip }])
            .then(function()
            {
                if (data.username != username)
                {
                    let transaction = db.transaction('users', 'readwrite');
                    let objectStore = transaction.objectStore('users');
                    let request = objectStore.get(userip);

                    request.onerror = function(event)
                    {
                        console.log("Looks like the get couldn't be gotten: " + event.target.errorCode);
                    };

                    request.onsuccess = function(event)
                    {
                        let data = event.target.result;
                        data.username = username;

                        let requestUpdate = objectStore.put(data);

                        requestUpdate.onerror = function(event)
                        {
                            console.log(event.target.error.name);
                            if (event.target.error.name == 'ConstraintError')
                            {
                                alert("you're not allowed to post using someone else's username. Change it. Also, if you can read this it means the hash didn't work.");
                            }
                        };
                        requestUpdate.onsuccess = function(event)
                        {
                            location.reload();
                        };
                    };
                }
                else
                {
                    location.reload();
                }
            });
        }
        else if(hash == null && newUser)
        {
            create('comments', [{ content: comment, userid: userip }])
            .then(function() {
                location.reload();
            });
        }
        else
        {
            alert("looks like you're trying to post with someone else's username, you'll have to put in the correct name.");
        }
    });
};

function deleteDatabase()
{
    indexedDB.deleteDatabase(dbName);
    setTimeout(function()
    {
        location.replace('elect.html');
    }, 1000);
};

function checkGetAfterLoad()
{
    if (typeof($_GET.candidate) != 'undefined')
    {
        vote($_GET.candidate);
    }
    if (typeof($_GET.delete) != 'undefined')
    {
        deleteComment($_GET.delete);
    }
};

/*
function deleteComment(id)
{
    deleteOne('comments', parseInt(id))
    .then(function()
    {
        window.location.replace('elect.html');
    });
};*/

function deleteComment(get)
{
    eval("let request = db.transaction(['comments'], 'readwrite').objectStore('comments').delete(".concat(get,");request.onsuccess = function(){location.replace('elect.html');};"));
    console.log(get);
}

String.prototype.hashCode = function(){
	var hash = 0;
	if (this.length == 0) return hash;
	for (i = 0; i < this.length; i++) {
		char = this.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
};

function createHash()
{
    readByValue('users', 'ip', userip)
    .then(function(data)
    {
        let hash = document.createElement('input');
        let main = document.getElementsByTagName('main')[0];
        hash.type = 'hidden';
        hash.id = 'hash';
        hash.value = (data.username + data.ip).hashCode();
        
        main.appendChild(hash);
    });
}